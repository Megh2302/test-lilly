<?php
function getPossibleWords($numberSequence){
    $numseq = (string)$numberSequence;
    if (strlen($numseq) == 0) {
        return array();
    }
    //shorthand array is only available after php 5.4
    $mappings = array(
        '0' => array(""), '1' => array(""), //to skip 1 & 0 that do not correspond with any letter in telephone 10-digit keypad.
        '2' => array("a", "b", "c"), '3' => array("d", "e", "f"), '4' => array("g", "h", "i"),
        '5' => array("j", "k", "l"), '6' => array("m", "n", "o"), '7' => array("p", "q", "r", "s"),
        '8' => array("t", "u", "v"), '9' => array("w", "x", "y", "z"));
    $combinations = array("");
    // for digits ('235' => 3)
    for ($i = 0; $i < strlen($numseq);$i++) {
        $number = $numseq[$i]; $letters = $mappings[$number]; $temp = array();
        // for letters ('pqrs' => 4)
        for ($j = 0; $j < sizeof($letters); $j++) {
            $newletter = $letters[$j];
            // for existing combinations (['dj', 'ej', 'fj'] => 3 count)
            for ($m = 0; $m < sizeof($combinations); $m++) {
                $combination = $combinations[$m];
                array_push($temp, $combination . $newletter);
            }
        }
        $combinations = $temp;
    }
    //return combinations in array format
    return print_r($combinations);
}
getPossibleWords(232);
?>
